'use strict';
const createRouter = require('@arangodb/foxx/router');
const router = createRouter();
const joi = require('joi');
const db = require('@arangodb').db;
const error = require('@arangodb').errors;
const DOC_NOT_FOUND = error.ERROR_ARANGO_DOCUMENT_NOT_FOUND.code;
const foxxColl = db._collection('Investor_details');

module.context.use(router);
 if(!db._collection('Investor_details')) {
    db._createDocumentCollection('Investor_details');
    console.log('ConsoleLog');
}

router.post('/finetch',function(req,res) {
    const data = req.body;
    console.log("Bodyy>>>>"+req.body);
   const meta=foxxColl.save(req.body);
   res.send(meta);
})
.body(joi.object().required(), 'Entry to store in the collection.')
.response(joi.object().required(), 'Entry stored in the collection.')
.summary('Store an entry')
.description('Stores an entry in the "myFoxxCollection" collection.');


router.get('/entries/:key', function (req, res) {
    try {
      const data = foxxColl.document(req.pathParams.key);
      res.send(data);
    } catch (e) {
      if (!e.isArangoError || e.errorNum !== DOC_NOT_FOUND) {
        throw e;
      }
      res.throw(404, 'The entry does not exist', e);
    }
  })
  .pathParam('key', joi.string().required(), 'Key of the entry.')
  .response(joi.object().required(), 'Entry stored in the collection.')
  .summary('Retrieve an entry')
  .description('Retrieves an entry from the "Investor_details" collection by key.');

  router.put('/updateInvestor',function(req,res) {
    const data = req.body;
    console.log("Bodyy>>>>"+req.body);
  //  const meta=foxxColl.save(req.body);
   res.send(data);
})
.body(joi.object().required(), 'Entry to store in the collection.')
.response(joi.object().required(), 'Entry stored in the collection.')
.summary('Store an entry')
.description('Stores an entry in the "myFoxxCollection" collection.');

  router.delete('/deleteInvestor/:key', function (req, res) {
    try {
        const data =req.pathParams.key;
        console.log(">>Data>>>"+data);
//      const data = foxxColl.document(req.pathParams.key);
       res.send(data);
    } catch (e) {
      if (!e.isArangoError || e.errorNum !== DOC_NOT_FOUND) {
        throw e;
      }
      res.throw(404, 'The entry does not exist', e);
    }
  })
  .pathParam('key', joi.string().required(), 'Key of the entry.')
  .response(joi.object().required(), 'Delete Content collection.')
  .summary('update Collections')
  .description('update an entry from the "Investor_details" collection by key.');







// router.get('/hello-world',function(req,res){
//     res.send('Hello-Mumbai');
// })
// .response(['text/plain'],'A generic gretting.');
// router.get('/hello/:name',function(req,res){
    
// res.send(`Ka hoo Kaa haal baa : ${req.pathParams.name}`);

// })
// .pathParam('name',joi.string().required(),'Name to  greet')
// .response(['text/plain'],'A personlized gretting')
// .summary('Personalized greeting')
// .description('Prints a personalized greeting.');

// router.get('/secrets',function(req,res,next){

// if(req.arangoUser){
//     console.log('User>>>>>>>>>'+req.arangoUser);
//     next();
// } else{
//     res.throw(404,'Invalid user');
// }
// },function(req,res){
//     console.log('allourSecrt zip');
// }
// );
// router.post('/sum',function(req,res){
//     const values = req.body.values;
//     res.send({
//       sum: values.reduce(function(a,b){
//           return a+b;
//         },0)
//     });
// })
// .body(joi.object({
//     values: joi.array().items(joi.number().required()).required()
//   }).required(), 'Values to add together.')
//   .response(joi.object({
//     result: joi.number().required()
//   }).required(), 'Sum of the input values.')
//   .summary('Add up numbers')
//   .description('Calculates the sum of an array of number values.');


// const db = require('@arangodb').db;
// console.log("Db>>>>>>>>>>>"+db);
// const collectionName = 'Investor';
// const isConnection =db._collection(collectionName);
// console.log("Collevtionnn"+isConnection);
// if(!isConnection){
//     db._createDocumentCollection(collectionName);
//     console.log('ConsoleLog');
// }
